# Balok-HJ: a data-race detector for HJ-lib

[Balok-HJ](https://gitlab.com/cogumbreiro/balok-hj/) is an extension to hj-lib
that monitors your HJ-lib program to detect data races.

Balok-HJ modifies the internals of the HJ-lib
runtime so that it can verify for data-race errors with [Balok](https://gitlab.com/cogumbreiro/balok/).

# Usage

1. **Use our HJ-lib JAR** `build/libs/hjlib-cooperative-0.1.11-SNAPSHOT+BALOK.jar` as a drop-in replacement of the hj-lib distribution
  runtime augmented race-detection (if you use Maven/Gradle see below). 

2. **Pass the JVM option `-Dhj.plugins=balok.hj.Main`** to verify a Java program.

# Build our HJ-lib JAR

Invoke `gradlew` to generate a verified hj-lib distribution JAR: 
```bash
$ ./gradlew 

> Task :dist
GENERATED: /home/tiago/Work/balok-hj/build/libs/hjlib-cooperative-0.1.11-SNAPSHOT+BALOK.jar


BUILD SUCCESSFUL in 5s
2 actionable tasks: 2 executed
```

# Your project uses Maven and Gradle

Run `./gradlew install` to install our JAR locally in your Maven repository.
```bash
$ ./gradlew install

> Task :weave
[ant:iajc] warning incorrect classpath: /home/tiago/Work/balok-hj/build/resources/main

> Task :dist
GENERATED: /home/tiago/Work/balok-hj/build/libs/hjlib-cooperative-0.1.11-SNAPSHOT+BALOK-1.0-dev.jar

> Task :install
Installed hjlib-cooperative-0.1.11-SNAPSHOT.jar in local Maven repository.

Gradle:

compile 't.cog.hjlib-cooperative.0.1.11-SNAPSHOT'

Maven:

<dependency>
    <groupId>t.cog</groupId>
    <artifactId>hjlib-cooperative</artifactId>
    <version>0.1.11-SNAPSHOT</version>
</dependency>


BUILD SUCCESSFUL in 5s
8 actionable tasks: 5 executed, 3 up-to-date
``` 

# TODO

- [x] The generated JAR should be in a Maven repository.
- [ ] The generated JAR should be in a remote Maven repository.
- [ ] Add a full example to the README (including the full command line options).
