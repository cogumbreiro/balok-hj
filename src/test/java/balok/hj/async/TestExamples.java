package balok.hj.async;

import balok.hj.ActivityTracker;
import balok.hj.AppProperties;
import balok.hj.DetectionStrategy;
import balok.causality.AccessMode;
import org.junit.Test;

import java.util.concurrent.ExecutionException;

import static org.junit.Assert.fail;

public class TestExamples {
    @Test
    public void happyPath() throws InterruptedException, ExecutionException {
        try {
            AppProperties.setDetectionStrategy(DetectionStrategy.ASYNC);
            AppProperties.getDetectionStrategy().start();
            ActivityTracker task1 = new ActivityTracker();
            AsyncShadowLocation loc = new AsyncShadowLocation();


            ActivityTracker task2 = task1.createChild();

            // produce an event and an access
            task1.afterSpawn();
            task1.onAccess(loc, AccessMode.WRITE, 0);
            task1.onEndActivity();

            // child performing a concurrent access
            task2.onAccess(loc, AccessMode.WRITE, 0);
            task2.onEndActivity();

            AppProperties.getDetectionStrategy().stop().get();
            fail("No race detected!");
        } catch (ExecutionException | IllegalStateException e) {
            // OK
        }
    }
}
