package examples;

import edu.rice.hj.Module1;
import edu.rice.hj.api.HjFuture;
import edu.rice.hj.api.HjSuspendable;
import edu.rice.hj.api.SuspendableException;
import edu.rice.hj.runtime.config.HjSystemProperty;
import balok.hj.Main;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import static edu.rice.hj.Module0.*;
import static org.junit.Assert.*;

/**
 * @author Tiago Cogumbreiro, Rishi Surendran
 */

@RunWith(JUnit4.class)
public class TestNoRace0 {

    private static class Cell {
        int x;
        int y;
    }

    @Before
    public void beforeMethod() {
        if (!Main.isEnabled()) {
            System.err.println("To enable race-detection you must start the JVM with the following option: -Dhj.plugins=" + Main.class.getName());
        }
        org.junit.Assume.assumeTrue(Main.isEnabled());
    }

    @After
    public void tearDown() {
        HjSystemProperty.resetConfigurations();
    }

    @Test
    public void readAfterWrite() {
        launchHabaneroApp(() -> {
            final Cell cell = new Cell();
            // anonymous region
            finish(() -> {
                asyncNb(() -> {
                    cell.x = 1000;
                });
                asyncNb(() -> {
                     cell.y = 2000;
                });
            });
            assertEquals(3000, cell.x + cell.y);
        });
    }

    @Test
    public void futures() {
        launchHabaneroApp(() -> {
            final Cell cell = new Cell();
            final HjFuture<?> fx = futureNb(() -> {
                cell.x = 1000;
                return null;
            });
            final HjFuture<?> fy = futureNb(() -> {
                cell.y = 2000;
                return null;
            });
            fx.get();
            fy.get();
            assertEquals(3000, cell.x + cell.y);
        });

    }


    @Test
    public void nestedFinish1() {
        launchHabaneroApp(() -> {
            final Cell cell = new Cell();
            finish(() -> {
                asyncNb(() -> {
                    asyncNb(() -> {
                        cell.x = 10;
                    });
                });
            });
            asyncNb(() -> {
                assertEquals(10, cell.x);
            });
        }
        );
    }

    @Test
    public void nestedFinish2() {
        launchHabaneroApp(() -> {
                    final Cell cell = new Cell();
                    finish(() -> {
                        Module1.async(new HjSuspendable() {
                            public void run() throws SuspendableException {
                                finish(() -> {
                                    asyncNb(() -> {
                                        cell.x = 10;
                                    });
                                });
                            }
                        });
                    });
                    asyncNb(() -> {
                        assertEquals(10, cell.x);
                    });
                }
        );
    }

    @Test
    public void childParent2() {
        launchHabaneroApp(() -> {
            final Cell cell = new Cell();
            cell.x = 2000;
            asyncNb(() -> {
                cell.x = 1000;
            });
        });
    }
}