package examples;

import edu.rice.hj.api.HjFuture;
import edu.rice.hj.runtime.forkjoin.ForkJoinRuntime;
import edu.rice.hj.runtime.config.HjSystemProperty;
import balok.causality.TaskView;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import static edu.rice.hj.Module0.*;
import static org.junit.Assert.*;

/**
 * @author Tiago Cogumbreiro, Rishi Surendran
 */
@RunWith(JUnit4.class)
public class TestInstrumentation {
    @After
    public void tearDown() {
        HjSystemProperty.resetConfigurations();
    }

    public static void assertHB(TaskView t1, TaskView t2) {
        assertHB("", t1, t2);
    }

    public static void assertHB(String message, TaskView t1, TaskView t2) {
        if (!t2.observe(t1.project()).isPresent()) {
            if (t1.observe(t2.project()).isPresent()) {
                fail(message + ": Expected " + t1 + " HB " + t2 + ", but got " + t2 + " HB " + t1);
            } else {
                fail(message + ": Expected " + t1 + " HB " + t2 + ", but got " + t1 + " PAR " + t2);
            }
        }
    }

    public static void assertPAR(TaskView t1, TaskView t2) {
        assertPAR("", t1, t2);
    }

    public static void assertPAR(String message, TaskView t1, TaskView t2) {
        if (t2.observe(t1.project()).isPresent()) {
            if (t2.getLocal().observe(t1.project().getLocal()).isPresent()) {
                fail(message + " because local is ordered: " + t1.getLocal() + " < " + t2.getLocal());
            }
            if (t1.getGlobal().happensBefore(t2.getGlobal())) {
                fail(message + " because global is ordered: " + t1.getGlobal() + " < " + t2.getGlobal());
            }
            if (t1.getGlobalTime() < t2.getGlobalTime()) {
                fail(message + " because global time is ordered: " + t1.getGlobalTime() + " < " + t2.getGlobalTime());
            }
            throw new IllegalStateException("Should not reach here!");
        }

        if(t1.observe(t2.project()).isPresent()) {
            if (t1.getLocal().observe(t2.project().getLocal()).isPresent()) {
                fail(message + " because local is ordered: " + t2.getLocal() + " < " + t1.getLocal());
            }
            if (t2.getGlobal().happensBefore(t1.getGlobal())) {
                fail(message + " because global is ordered: " + t2.getGlobal() + " < " + t1.getGlobal());
            }
            if (t2.getGlobalTime() < t1.getGlobalTime()) {
                fail(message + " because global time is ordered: " + t2.getGlobalTime() + " < " + t1.getGlobalTime());
            }
            throw new IllegalStateException("Should not reach here!");
        }
    }

    private static TaskView getCurrentTimestamp() {
        return ForkJoinRuntime.currentHabaneroActivity().tracker.createTimestamp();
    }

    @Test
    public void finishWorks() {
        launchHabaneroApp(() -> {
            final TaskView s0 = getCurrentTimestamp();
            finish( () -> {
                final TaskView s1 = getCurrentTimestamp();
                assertHB(s0, s1);
            });
        });
    }

    @Test
    public void futures() {
        launchHabaneroApp(() -> {
            TaskView s0 = getCurrentTimestamp();
            HjFuture<TaskView> fx = futureNb(() -> getCurrentTimestamp());
            TaskView s1 = getCurrentTimestamp();
            TaskView sx = fx.get();
            TaskView s2 = getCurrentTimestamp();
            assertHB("s0 <= s1", s0, s1);
            assertPAR("s1 || sx", s1, sx);
            assertHB("s0 <= sz", s0, sx);
            assertHB("s1 <= s2", s1, s2);
            assertHB("sx <= s2", sx, s2);
        });
    }

    @Test
    public void futures2() {
        launchHabaneroApp(() -> {
            TaskView s0 = getCurrentTimestamp();
            final HjFuture<TaskView> fx = futureNb(() -> getCurrentTimestamp());
            final HjFuture<TaskView> fy = futureNb(() -> getCurrentTimestamp());
            TaskView s1 = getCurrentTimestamp();
            TaskView sx = fx.get();
            TaskView s2 = getCurrentTimestamp();
            TaskView sy = fy.get();
            TaskView s3 = getCurrentTimestamp();
            // as before
            assertPAR("s1 || sx", s1, sx);
            assertHB("s0 < s1", s0, s1);
            assertHB("s0 < sx", s0, sx);
            assertHB("s1 < s2", s1, s2);
            assertHB("sx < s2", sx, s2);
            assertPAR("sx || sy", sx, sy);
            assertPAR("sy || s1", sy, s1);
            assertPAR("s2 || sy", s2, sy);
            assertHB("sy < s3", sy, s3);
        });
    }

    @Test
    public void futureNb0() {
        for (int i = 0; i < 1000; i++) {
            launchHabaneroApp(() -> {
                TaskView s0 = getCurrentTimestamp();
                final HjFuture<TaskView> fx = futureNb(() -> getCurrentTimestamp());
                assertHB(s0, getCurrentTimestamp());
            });
        }
    }

    @Test
    public void futureNb1() {
        for (int i = 0; i < 100; i++) {
            launchHabaneroApp(() -> {
                final HjFuture<TaskView> fx = futureNb(() -> getCurrentTimestamp());
                TaskView s0 = getCurrentTimestamp();
                fx.get();
                assertHB(s0, getCurrentTimestamp());
            });
        }
    }

    @Test
    public void futureNb2() {
        for (int i = 0; i < 100; i++) {
            final int I = i;
            launchHabaneroApp(() -> {
                // 1
                final HjFuture<TaskView> fx = futureNb(() -> null);
                // 2
                final HjFuture<TaskView> fy = futureNb(() -> null);
                // 3
                TaskView s3 = getCurrentTimestamp();
                fx.get();
                // 4
                TaskView s4 = getCurrentTimestamp();
                assertHB("ITER " + I + " s3 < s4", s3, s4);
            });
        }
    }

    @Test
    public void futureNb3() {
        for (int i = 0; i < 100; i++) {
            launchHabaneroApp(() -> {
                final HjFuture<TaskView> fx = futureNb(() -> getCurrentTimestamp());
                final HjFuture<TaskView> fy = futureNb(() -> getCurrentTimestamp());
                fx.get();
                TaskView sy = fy.get();
                TaskView s3 = getCurrentTimestamp();
                assertHB(sy, s3);
            });
        }
    }

    @Test
    public void childParent2() {
        launchHabaneroApp(() -> {
            final TaskView[] T = new TaskView[2];
            finish( () -> {
                T[0] = getCurrentTimestamp();
                asyncNb(() -> {
                    T[1] = getCurrentTimestamp();
                });
            });
            assertHB("", T[0], T[1]);
        });
    }
}