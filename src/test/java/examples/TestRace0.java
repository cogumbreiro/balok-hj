package examples;


import edu.rice.hj.api.HjSuspendable;
import edu.rice.hj.api.SuspendableException;
import balok.hj.Main;
import edu.rice.hj.runtime.config.HjSystemProperty;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.util.Vector;

import static edu.rice.hj.Module0.*;
import static edu.rice.hj.Module1.*;
import static org.junit.Assert.*;

/**
 * @author Tiago Cogumbreiro, Rishi Surendran
 */

@RunWith(JUnit4.class)
public class TestRace0 {

    private static class Cell {
        int x;
        int y;
    }

    @Before
    public void beforeMethod() {
        if (!Main.isEnabled()) {
            System.err.println("To enable race-detection you must start the JVM with the following option: -Dhj.plugins=" + Main.class.getName());
        }
        org.junit.Assume.assumeTrue(Main.isEnabled());
    }

    @After
    public void tearDown() {
        HjSystemProperty.resetConfigurations();
    }

    @Test
    public void example1() {
        final Cell cell = new Cell();
        final Vector<RuntimeException> exceptions = new Vector<>();
        launchHabaneroApp(() -> {
            // anonymous region
            asyncNb(() -> {
                try {
                    cell.x = 1000;
                } catch (RuntimeException e) {
                    exceptions.add(e);
                }
            });
            asyncNb(() -> {
                try {
                    cell.x = 2000;
                } catch (RuntimeException e) {
                    exceptions.add(e);
                }
            });
        });
        assertEquals("Expected races", 1, exceptions.size());
    }

    @Test
    public void testMethod() {
        launchHabaneroApp(() -> {
            final Cell cell = new Cell();
            final Vector<RuntimeException> exceptions = new Vector<>();

            // anonymous region
            finish(() -> {
                asyncNb(() -> {
                    try {
                        cell.x = 1000;
                    } catch (RuntimeException e) {
                        exceptions.add(e);
                    }
                });
                asyncNb(() -> {
                    try {
                        cell.y = 2000;
                    } catch (RuntimeException e) {
                        exceptions.add(e);
                    }
                });
                int result = 0;
                try {
                    result += cell.x;
                } catch (RuntimeException e) {
                    exceptions.add(e);
                }
                try {
                    result += cell.y;
                } catch (RuntimeException e) {
                    exceptions.add(e);
                }
            });
            assertEquals("Expected 2 races", 2, exceptions.size());
        });
    }

    @Test
    public void nestedFinish2() {
        // races are not influenced by finish scopes
        launchHabaneroApp(() -> {
            final Vector<RuntimeException> exceptions = new Vector<>();
            final Cell cell = new Cell();
            finish(() -> {
                async(new HjSuspendable() {
                    public void run() throws SuspendableException {
                        finish(() -> {
                            asyncNb(() -> {
                                try {
                                    cell.x = 10;
                                } catch (RuntimeException e) {
                                    exceptions.add(e);
                                }
                            });
                        });
                    }
                });
                async(new HjSuspendable() {
                    public void run() throws SuspendableException {
                        finish(() -> {
                            asyncNb(() -> {
                                try {
                                    cell.x = 20;
                                } catch (RuntimeException e) {
                                    exceptions.add(e);
                                }
                            });
                        });
                    }
                });
            });
            assertEquals(exceptions.size(), 1);
        });
    }

    @Test
    public void childParent1() {
        launchHabaneroApp(() -> {
            final Cell cell = new Cell();
            final Vector<RuntimeException> exceptions = new Vector<>();
            finish(()-> {
                async(() -> {
                    try {
                        cell.x = 2000;
                    } catch (RuntimeException e) {
                        exceptions.add(e);
                    }
                });
                // anonymous region
                finish(() -> {
                    asyncNb(() -> {
                        try {
                            cell.x = 1000;
                        } catch (RuntimeException e) {
                            exceptions.add(e);
                        }
                    });
                });
            });
            assertEquals("Expected 1 race", 1, exceptions.size());
        });
    }

}