package examples;

import edu.rice.hj.runtime.config.HjSystemProperty;
import balok.hj.Main;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import static edu.rice.hj.Module0.*;
import static org.junit.Assert.assertEquals;

/**
 * @author Tiago Cogumbreiro, Rishi Surendran
 */

@RunWith(JUnit4.class)
public class Test2DArray {

    @Before
    public void beforeMethod() {
        if (!Main.isEnabled()) {
            System.err.println("To enable race-detection you must start the JVM with the following option: -Dhj.plugins=" + Main.class.getName());
        }
        org.junit.Assume.assumeTrue(Main.isEnabled());
    }

    @After
    public void tearDown() {
        HjSystemProperty.resetConfigurations();
    }

    @Test
    public void bigArray() {
        launchHabaneroApp(() -> {
            final int[] result = {0};
            final int[][] input = new int[10][10];
            final int[] output = new int[10];
            finish(() -> {
                finish(() -> {
                    for (int i = 0; i < 10; i++) {
                        final int I = i;
                        asyncNb(() -> {
                            for (int j = 0; j < 10; j++)
                                input[I][j] = I + j;
                        });
                    }
                });
                finish(() -> {
                    for (int i = 0; i < 10; i++) {
                        final int I = i;
                        asyncNb(() -> {
                            for (int j = 0; j < 10; j++)
                                output[I] += input[I][j];
                        });
                    }
                });
                asyncNb(() -> {
                    for (int i = 0; i < 10; i++)
                        result[0] += output[i];
                });

            });
            assertEquals(900, result[0]);
        });
    }

    @Test
    public void smallArray() {
        launchHabaneroApp(() -> {
            final int[] result = {0};
            final int L = 3;
            final int R = 2;
            final int[][] input = new int[L][R];
            final int[][] output = new int[L][R];
            finish(() -> {
                for (int i = 0; i < L; i++) {
                    final int I = i;
                    asyncNb(() -> {
                        for (int j = 0; j < R; j++)
                            input[I][j] = 1;
                    });
                }
            });
            finish(() -> {
                for (int i = 0; i < L; i++) {
                    final int I = i;
                    asyncNb(() -> {
                        for (int j = 0; j < R; j++)
                            output[I][j] += input[I][j];
                    });
                }
            });
            int sum = 0;
            for (int i = 0; i < L; i++) {
                for (int j = 0; j < R; j++) {
                    sum += output[i][j];
                }
            }
            assertEquals(L*R, sum);
        });
    }
}