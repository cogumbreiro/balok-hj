package edu.rice.hj.runtime.config;

public class Helper {
    public static boolean isHjRunning() {
        return HjConfiguration.runtimeStarted();
    }
}
