package balok.hj;

import edu.rice.hj.api.HjRuntime;
import edu.rice.hj.continuation.instrument.MethodDatabase;
import edu.rice.hj.runtime.config.Helper;
import edu.rice.hj.runtime.config.HjPlugin;
import edu.rice.hj.runtime.forkjoin.ForkJoinRuntime;
import balok.causality.AccessMode;
import org.objectweb.asm.ClassVisitor;
import t.cog.strigoi.*;

import java.util.function.Predicate;
import java.util.function.UnaryOperator;

import static t.cog.strigoi.Instrumentation.asClassFileTransformer;

public class Main {
    private static volatile boolean isEnabled;

    public synchronized static void setEnabled() {
        isEnabled = true;
    }

    public synchronized static void clearEnabled() {
        isEnabled = false;
    }

    public static synchronized boolean isEnabled() {
        return isEnabled;
    }

    private final static DebugDB DEBUG_DB = new DebugDB();

    private static String[] IGNORED = {
            // Java SE
            "java/",
            "javax/",
            "sun/",
            "com/sun/",
            "org/openjdk/",
            // 3rd-party
            "org/gradle/",
            "org/slf4j",
            "org/junit/",
            "org/apache/",
            "org/hamcrest/",
            "org/objectweb/",
            "it/unimi/",
            "org/aspectj/",
            "org/apache/",
            "worker/org/gradle/",
            "net/rubygrapefruit/",
            "com/esotericsoftware/",
            "com/carrotsearch/",
            "com/intellij/",
            "joptsimple/",
            "com/ankurdave/part/",
            "com/google/common/",
            "io/vavr/",
            "org/jctools",
            // HJ
            "edu/rice/hj/Module",
            "edu/rice/hj/runtime/",
            "edu/rice/hj/api/",
            "edu/rice/hj/experimental/api/",
            "edu/rice/hj/continuation/",
            "edu/rice/hj/actors",
            "edu/rice/hj/selectors",
            // our software
            "gorn/",
            "balok/",
    };

    private static final Predicate<String> instrumentClasses = className -> {
        for (String pkg : IGNORED) {
            if (className.startsWith(pkg)) {
                return false;
            }
        }
        // Do not instrument generated classes JMH
        if (className.endsWith("_run_jmhTest")) {
            return false;
        }
        return true;
    };
    private static final Predicate<FieldLocation> fieldSelection = f -> {
        boolean result = instrumentClasses.test(f.getClassName()) && f.getFieldName().indexOf('$') == -1;
        return result;
    };

    private static volatile DetectionStrategy strategy;

    public static final HjPlugin PLUGIN = new HjPlugin() {
        @Override
        public void onPreInstrumentation(String agentArguments, java.lang.instrument.Instrumentation instrumentation, MethodDatabase db) {
            if (AppProperties.isHelpEnabled()) {
                AppProperties.printOptions();
            }
            strategy = AppProperties.getDetectionStrategy();
            strategy.setDebugDB(DEBUG_DB);
            try {
                UnaryOperator<ClassVisitor> fieldVis = InstrumentFieldStateAccess
                        .make(ShadowLocation.class)
                        .setDebugDB(DEBUG_DB)
                        .fieldSelection(fieldSelection)
                        .onInit(Main.class, "createShadowVar")
                        .onAccess(Main.class, "onAccess", ParameterType.IS_PUT, ParameterType.DEBUG_INFO)
                        .buildClassVisitor();
                UnaryOperator<ClassVisitor> arrVis = InstrumentArrayAccess
                        .make()
                        .setDebugDB(DEBUG_DB)
                        .onInit(Main.class, "createShadowVar")
                        .onAccess(Main.class, "onArrayAccess", ParameterType.IS_PUT, ParameterType.DEBUG_INFO)
                        .buildClassVisitor();
                instrumentation.addTransformer(asClassFileTransformer(fieldVis, instrumentClasses));
                instrumentation.addTransformer(asClassFileTransformer(arrVis, instrumentClasses));
            } catch (NoSuchMethodException e) {
                throw new IllegalStateException(e);
            }
            System.out.println("*************************** BALOK LOADED ***************************");
            setEnabled();
        }
  
        @Override
        public void onInit(HjRuntime runtime) {}

    };
    public static final ActivityTracker getCurrentActivity() {
        return ForkJoinRuntime.currentHabaneroActivity().tracker;
    }

    public static final ShadowLocation createShadowVar() {
        return strategy.createShadowMemory();
    }

    public static final void createShadowVar(Object arr) {
        // Nothing to do
    }

    public static final void onArrayAccess(Object arr, int idx, boolean isPut, int debugId) {
        if (Helper.isHjRunning()) {
            getCurrentActivity().onArrayAccess(arr, idx, isPut ? AccessMode.WRITE : AccessMode.READ, debugId);
        }
    }

    public static final void onAccess(ShadowLocation var, boolean isPut, int debugId) {
        if (Helper.isHjRunning()) {
            getCurrentActivity().onAccess(var, isPut ? AccessMode.WRITE : AccessMode.READ, debugId);
        }
    }
}
