package balok.hj;

import com.carrotsearch.hppc.ObjectObjectIdentityHashMap;
import edu.rice.hj.api.HjPhaser;
import edu.rice.hj.api.HjPhaserMode;
import edu.rice.hj.api.HjPhaserPair;
import balok.causality.*;
import balok.vc.RegistrationMode;

import java.util.List;
import java.util.function.Supplier;

/**
 * @author Tiago Cogumbreiro
 */
public class ActivityTracker {
    private final TaskTracker task;
    private final ObjectObjectIdentityHashMap<HjPhaser, PhaserTracker> phasers = new ObjectObjectIdentityHashMap<>();
    private final FinishTracker finishes;
    private final MemoryTracker memTracker;
    private final Supplier<MemoryTracker> memFactory;
    private static final ShadowLocationMap<ShadowLocation> ARRAYS = new ShadowLocationMap<>();

    /**
     * The constructor should only be invoked by the root activity.
     */
    public ActivityTracker() {
        this(new TaskTracker(AppProperties.getClockStrategy().createController()), AppProperties.getDetectionStrategy().createTracker());
    }

    /**
     * Initializes with a specific tracker. Used internally or for testing only.
     * @param tracker
     */
    protected ActivityTracker(TaskTracker tracker, Supplier<MemoryTracker> memTracker) {
        this.task = tracker;
        this.finishes = new FinishTracker(tracker);
        this.memFactory = memTracker;
        this.memTracker = memTracker.get();
    }

    /**
     * Must be invoked when spawning an activity.
     * @return
     */
    public ActivityTracker createChild() {
        return new ActivityTracker(task.createChild(), memFactory);
    }

    public void produceEvent() {
        task.produceEvent();
        notifyMemTracker();
    }

    public TaskView createTimestamp() {
        return task.createTimestamp();
    }

    public void onStartFinish() {
        finishes.pushFinish();
    }

    public void onEndFinish() {
        finishes.popFinish();
        notifyMemTracker();
    }

    private void notifyMemTracker() {
        memTracker.onSyncEvent(task);
    }

    public void onEndActivity() {
        memTracker.onEnd(task);
    }

    public void onAccess(ShadowLocation loc, AccessMode mode, int debugId) {
        memTracker.onAccess(task, loc, mode, debugId);
    }

    public void onArrayAccess(Object arr, int idx, AccessMode mode, int debugId) {
        ShadowLocation loc = ARRAYS.get(arr, idx, memTracker::createLocation);
        memTracker.onAccess(task, loc, mode, debugId);
    }

    /**
     * Merge the given timestamp with the activity's causality and produce an event.
     * @param other
     */
    public void join(TaskView other) {
        this.task.join(other);
        notifyMemTracker();
    }

    /**
     * Must be invoked after createChild().
     */
    public void afterSpawn() {
        task.afterSpawn();
        notifyMemTracker();
    }


    /**
     * Creates a phaser tracking object.
     * @param phaser
     */
    public void createPhaser(HjPhaser phaser) {
        phasers.put(phaser, task.createPhaser());
    }

    /**
     * Convert a Hj registration mode to the abstract registration mode.
     * @param mode
     * @return
     */
    private static RegistrationMode adapt(HjPhaserMode mode) {
        switch(mode) {
        case SIG:
            return RegistrationMode.SIGNAL;
        case WAIT:
            return RegistrationMode.WAIT;
        case SIG_WAIT_SINGLE:
        case SIG_WAIT:
            return RegistrationMode.SIGNAL_WAIT;
        }
        throw new IllegalArgumentException(mode.toString());
    }

    /**
     * Register this task on the parent's phasers, according to some registration
     * modes.
     * @param parent
     * @param phaserPairList
     */
    public void register(ActivityTracker parent, List<HjPhaserPair> phaserPairList) {
        for (HjPhaserPair pair : phaserPairList) {
            PhaserTracker parentPhaser = parent.phasers.get(pair.phaser);
            // register this task on parent's phaser
            PhaserTracker phaser = task.registerPhaser(parentPhaser, adapt(pair.mode));
            phasers.put(pair.phaser, phaser);
        }
    }

    /**
     * Signal on the target phaser.
     * @param phaser
     */
    public void signal(HjPhaser phaser) {
        phasers.get(phaser).signal();
        notifyMemTracker();
    }

    /**
     * Awaits on the target phaser.
     * @param phaser
     */
    public void await(HjPhaser phaser) {
        PhaserTracker barrierTracker = phasers.get(phaser);
        if (barrierTracker == null) {
            throw new IllegalStateException("Phaser is not registered: " + phaser);
        }
        barrierTracker.await();
        notifyMemTracker();
    }
}
