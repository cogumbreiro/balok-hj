package balok.hj;

import balok.causality.AccessMode;
import balok.causality.TaskTracker;

public interface MemoryTracker {
    /**
     * Invoked whenever a task accesses a shared location.
     * @param tracker
     * @param loc
     * @param mode
     * @param debugId
     */
    void onAccess(TaskTracker tracker, ShadowLocation loc, AccessMode mode, int debugId);

    ShadowLocation createLocation();

    /**
     * Invoked whenever the task emits a synchronization event (thus updates its view).
     * @param tracker
     */
    void onSyncEvent(TaskTracker tracker);

    /**
     * Invoked whenever a task terminates its execution.
     * @param task
     */
    void onEnd(TaskTracker task);
}
