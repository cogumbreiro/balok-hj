package balok.hj.async;

import balok.hj.HjAccess;
import balok.hj.ShadowLocation;
import balok.causality.Epoch;
import balok.causality.async.AsyncLocationTracker;

import java.util.concurrent.atomic.AtomicInteger;

public class AsyncShadowLocation implements ShadowLocation {
    AsyncLocationTracker<HjAccess, Epoch> loc = new AsyncLocationTracker<>();
}
