package balok.hj.async;

import balok.hj.HjAccess;
import balok.hj.MemoryTracker;
import balok.hj.ShadowLocation;
import balok.causality.*;
import balok.causality.async.ShadowMemoryBuilder;
import balok.causality.async.ShadowMemory;
import org.jctools.queues.MpscUnboundedArrayQueue;

import java.util.*;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

public class AsyncMemoryTracker implements MemoryTracker {
    private static final AtomicInteger codeGen = new AtomicInteger(Integer.MIN_VALUE);
    private final int code = codeGen.getAndIncrement();
    private ShadowMemoryBuilder<HjAccess, Epoch> currentFrame = new ShadowMemoryBuilder<>(512);
    private AtomicBoolean active = new AtomicBoolean(true);

    private final MpscUnboundedArrayQueue<ShadowMemory> queue;

    public AsyncMemoryTracker(MpscUnboundedArrayQueue<ShadowMemory> queue) {
        this.queue = queue;
    }

    @Override
    public ShadowLocation createLocation() {
        return new AsyncShadowLocation();
    }

    @Override
    public void onAccess(TaskTracker tracker, ShadowLocation loc, AccessMode mode, int debugId) {
        // XXX: ADD DEBUGGING INFO
        AsyncShadowLocation key = (AsyncShadowLocation) loc;
        AccessEntry<HjAccess, Event<Epoch>> acc = new AccessEntry<>(new HjAccess(mode, debugId), tracker.createTimestamp());
        int ticket = key.loc.createTicket();
        if (!key.loc.tryAdd(acc, ticket)) {
            currentFrame.add(key.loc, acc, ticket);
            if (currentFrame.isFull()) {
                queue.add(currentFrame.build());
            }
        }
    }

    @Override
    public void onSyncEvent(TaskTracker tracker) {
        //push(tracker);
    }

    @Override
    public void onEnd(TaskTracker tracker) {
        ShadowMemory frame = currentFrame.isEmpty() ? null : currentFrame.build();
        if (frame != null) {
            queue.add(frame);
        }
        active.set(false);
    }

    public ArrayList<ShadowMemory<HjAccess, Epoch>> consume() {
        ArrayList<ShadowMemory<HjAccess, Epoch>> result = new ArrayList<>();
        queue.drain(result::add);
        return result;
    }

    @Override
    public int hashCode() {
        return code;
    }

    public boolean isRunning() {
        return active.get();
    }

    @Override
    public String toString() {
        return "(current=" + currentFrame + ")";
    }
}
