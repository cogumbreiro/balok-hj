package balok.hj.async;

import balok.hj.HjAccess;
import balok.hj.MemoryTracker;
import balok.causality.Epoch;
import balok.causality.async.ShadowMemory;
import org.jctools.queues.MpscUnboundedArrayQueue;

import java.util.*;
import java.util.concurrent.*;
import java.util.function.Supplier;

public class MemoryTrackerManager implements Supplier<MemoryTracker> {
    private ShadowMemory<HjAccess, Epoch> history;
    // Ensure that we do not have parallelism hence no concurrency
    private ScheduledExecutorService eventLoop;
    private MpscUnboundedArrayQueue<ShadowMemory> queue;
    private Future<?> running;
    /**
     * Create a memory tracker; this factory is invoked whenever a task is created.
     * @return
     */
    @Override
    public MemoryTracker get() {
        return new AsyncMemoryTracker(queue);
    }

    public void start() {
        history = new ShadowMemory<>();
        eventLoop = Executors.newScheduledThreadPool(1);
        queue = new MpscUnboundedArrayQueue<>(128);
        running = eventLoop.scheduleWithFixedDelay(() -> queue.drain(x -> history.addAll(x)),
                500, 200, TimeUnit.MILLISECONDS);
    }

    public Future<?> stop() {
        running.cancel(true); // no need to continue running
        return eventLoop.submit(() -> {
            queue.drain(history::addAll);
            eventLoop.shutdown();
        });
    }
}
