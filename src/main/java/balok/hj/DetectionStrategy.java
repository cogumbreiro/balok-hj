package balok.hj;

import balok.hj.async.AsyncShadowLocation;
import balok.hj.async.MemoryTrackerManager;
import t.cog.strigoi.DebugDB;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.function.Supplier;

public enum DetectionStrategy {

    SYNC {
        private DebugDB db;
        private final SyncMemoryTracker tracker = new SyncMemoryTracker();

        @Override
        public Supplier<MemoryTracker> createTracker() {
            return () -> tracker;
        }

        @Override
        public ShadowLocation createShadowMemory() {
            return new SyncShadowLocation(db);
        }

        @Override
        public void start() {
            // do nothing
        }

        @Override
        public Future<?> stop() throws InterruptedException {
            return IDLE;
        }

        @Override
        public void setDebugDB(DebugDB db) {
            this.db = db;
            this.tracker.setDb(db);
        }
    },
    ASYNC {

        private final MemoryTrackerManager mgr = new MemoryTrackerManager();

        @Override
        public Supplier<MemoryTracker> createTracker() {
            return mgr;
        }

        @Override
        public ShadowLocation createShadowMemory() {
            return new AsyncShadowLocation();
        }

        @Override
        public void start() {
            mgr.start();
        }

        @Override
        public Future<?> stop() throws InterruptedException {
            return mgr.stop();
        }

        @Override
        public void setDebugDB(DebugDB db) {

        }
    }
    ;
    private static final Future<?> IDLE = new Future<Object>() {
        @Override
        public boolean cancel(boolean mayInterruptIfRunning) {
            return true;
        }

        @Override
        public boolean isCancelled() {
            return false;
        }

        @Override
        public boolean isDone() {
            return true;
        }

        @Override
        public Object get() throws InterruptedException, ExecutionException {
            return null;
        }

        @Override
        public Object get(long timeout, TimeUnit unit) throws InterruptedException, ExecutionException, TimeoutException {
            return null;
        }
    };
    public abstract void start();
    public abstract Future<?> stop() throws InterruptedException;
    public abstract Supplier<MemoryTracker> createTracker();
    public abstract ShadowLocation createShadowMemory();
    public abstract void setDebugDB(DebugDB db);
}
