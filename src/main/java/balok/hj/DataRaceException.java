package balok.hj;

import balok.causality.*;
import t.cog.strigoi.DebugDB;

public class DataRaceException extends RuntimeException {
    public DataRaceException(AccessEntry<HjAccess, Epoch> prev, AccessEntry<HjAccess, Event<Epoch>> curr, DebugDB debugDB) {
        super(prettyPrint(prev, curr, debugDB));
    }
    private static String toString(AccessMode mode) {
        return mode + " " + (mode == AccessMode.WRITE ? "to" : "from") + " ";
    }
    private static String prettyPrint(AccessEntry<HjAccess, Epoch> prev, AccessEntry<HjAccess, Event<Epoch>> curr, DebugDB debugDB) {
        Object currLocation = debugDB.get(curr.getAccess().getDebugId());
        Object prevLocation = debugDB.get(prev.getAccess().getDebugId());
        String msg = "";
        msg += "Data Race: (curr) " + toString(curr.getAccess().getMode()) + " " + currLocation;
        msg += " / (prev) " + toString(prev.getAccess().getMode()) + " " + prevLocation;
        msg += "\n";
        msg += "Curr Location:  " + curr.getAccess().getMode() + " " + currLocation + "\n";
        msg += "Prev Location:  " + prev.getAccess().getMode() + " " + prevLocation + "\n";
        msg += "Curr Timestamp: " + curr.getValue() +"\n";
        msg += "Prev Timestamp: " + prev.getValue();
        return msg;
    }
}
