package balok.hj;

import com.carrotsearch.hppc.ObjectStack;
import balok.causality.AllToAllTracker;
import balok.causality.TaskTracker;

public class FinishTracker {
    private final TaskTracker task;
    ObjectStack<AllToAllTracker> finishes = new ObjectStack<>();
    // reuse the last barrier so that we can use a one-level finish as a cyclic barrier
    AllToAllTracker lastBarrier = null;
    public FinishTracker(TaskTracker tracker) {
        this.task = tracker;
    }

    public void pushFinish() {
        finishes.push(lastBarrier != null ? lastBarrier : task.createBarrier());
        lastBarrier = null;
    }

    public void popFinish() {
        lastBarrier = finishes.pop();
        lastBarrier.signalAwait();
    }
}
