package balok.hj;

import balok.causality.PtpCausalityFactory;

public class AppProperties {
    private final static String PREFIX = "hj.racedetection.";
    /**
     * Which causality algorithm should we use?
     * @see PtpCausalityFactory#values()
     */
    public static final String CLOCK_STRATEGY = PREFIX + "clock";
    /**
     * Is a data-race fatal? (Boolean)
     */
    public static final String FATAL = PREFIX + "fatal";

    /**
     * Print usage
     */
    public static final String HELP = PREFIX + "help";

    /**
     * Detection strategy
     */
    public static final String DETECTION_STRATEGY = PREFIX + "detection";

    public static PtpCausalityFactory getClockStrategy() {
        String name = System.getProperty(AppProperties.CLOCK_STRATEGY, PtpCausalityFactory.PREFIX.name());
        return PtpCausalityFactory.valueOf(name);
    }

    public static void setClockStrategy(PtpCausalityFactory factory) {
        System.setProperty(CLOCK_STRATEGY, factory.name());
    }

    public static DetectionStrategy getDetectionStrategy() {
        String name = System.getProperty(AppProperties.DETECTION_STRATEGY, DetectionStrategy.SYNC.name());
        return DetectionStrategy.valueOf(name);
    }

    public static void setDetectionStrategy(DetectionStrategy strategy) {
        System.setProperty(DETECTION_STRATEGY, strategy.name());
    }

    public static boolean isRaceFatal() {
        String prop = System.getProperty(FATAL, "false");
        return Boolean.parseBoolean(prop);
    }

    public static void setRaceFatal(boolean value) {
        System.setProperty(FATAL, Boolean.toString(value));
    }

    public static boolean isHelpEnabled() {
        String prop = System.getProperty(HELP, "false");
        return Boolean.parseBoolean(prop);
    }

    public static void printOptions() {
        System.out.println(CLOCK_STRATEGY + ": " + getClockStrategy());
        System.out.println(DETECTION_STRATEGY + ": " + getDetectionStrategy());
        System.out.println(FATAL + ": " + isRaceFatal());
        System.out.println(HELP + ": " + isHelpEnabled());
    }
}
