package balok.hj;

import balok.causality.AccessMode;
import balok.causality.TaskTracker;
import t.cog.strigoi.DebugDB;

public class SyncMemoryTracker implements MemoryTracker {
    private DebugDB db;

    public void setDb(DebugDB db) {
        this.db = db;
    }

    @Override
    public void onAccess(TaskTracker tracker, ShadowLocation loc, AccessMode mode, int debugId) {
        SyncShadowLocation shadowVar = (SyncShadowLocation) loc;
        shadowVar.add(tracker.createTimestamp(), mode, debugId);
    }

    @Override
    public void onSyncEvent(TaskTracker tracker) {
        // nothing to do
    }

    @Override
    public void onEnd(TaskTracker task) {
        // nothing to do
    }

    @Override
    public ShadowLocation createLocation() {
        return new SyncShadowLocation(db);
    }
}
