package balok.hj;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.function.Supplier;

public class ShadowLocationMap<L extends ShadowLocation> {
    private final ConcurrentReferenceHashMap<Object, ConcurrentMap<Integer,L>> arrays = new ConcurrentReferenceHashMap<>();

    public L get(Object arr, int idx, Supplier<L> factory) {
        ConcurrentMap<Integer,L> arrMap = arrays.get(arr);
        if (arrMap == null) {
            arrays.putIfAbsent(arr, new ConcurrentHashMap<>());
            arrMap = arrays.get(arr);
        }
        L loc = arrMap.get(idx);
        if (loc == null) {
            arrMap.putIfAbsent(idx, factory.get());
            loc = arrMap.get(idx);
        }
        return loc;
    }
}
