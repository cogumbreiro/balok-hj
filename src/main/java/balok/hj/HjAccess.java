package balok.hj;

import balok.causality.Access;
import balok.causality.AccessMode;

public final class HjAccess implements Access {
    private final AccessMode mode;
    private final int debugId;

    public HjAccess(AccessMode mode, int debugId) {
        this.mode = mode;
        this.debugId = debugId;
    }

    public int getDebugId() {
        return debugId;
    }

    @Override
    public AccessMode getMode() {
        return mode;
    }

    @Override
    public String toString() {
        return mode.toString();
    }
}
