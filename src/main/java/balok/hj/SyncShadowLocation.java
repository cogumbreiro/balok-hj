package balok.hj;

import balok.causality.*;
import t.cog.strigoi.DebugDB;

import java.util.concurrent.locks.ReentrantLock;

/**
 * The supertype of all types that can be stored in the shadow memory 
 * for an instance variable or array index.
 */
public class SyncShadowLocation implements ShadowLocation {
    private final DebugDB debugDB;

    private final LocationTracker<HjAccess, Epoch> tracker = new LocationTracker();

    private final ReentrantLock lock = new ReentrantLock();

    public SyncShadowLocation(DebugDB debugDB) {
        this.debugDB = debugDB;
    }

    /**
     * Add a memory access.
     */
    public void add(TaskView event, AccessMode mode, int debugId) {
        AccessEntry<HjAccess, Event<Epoch>> curr = new AccessEntry<>(new HjAccess(mode, debugId), event);
        final AccessEntry<HjAccess, Epoch> prev;
        if (tracker.lookupConflict(curr) != null) { // try a racy-check
            lock.lock();
            try {
                prev = tracker.lookupConflict(curr);
                if (prev != null) {
                    DataRaceException e = new DataRaceException(prev, curr, debugDB);
                    if (AppProperties.isRaceFatal()) {
                        System.err.println("*** DATA RACE DETECTED!");
                        e.printStackTrace();
                        System.exit(-1);
                    }
                    throw e;
                } else {
                    tracker.unsafeAdd(curr);
                }
            } finally {
                lock.unlock();
            }
        } else {
            lock.lock();
            try {
                tracker.unsafeAdd(curr);
            } finally {
                lock.unlock();
            }
        }
    }

}
