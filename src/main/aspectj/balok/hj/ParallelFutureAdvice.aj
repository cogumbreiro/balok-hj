package balok.hj;

import edu.rice.hj.api.*;
import edu.rice.hj.runtime.baseruntime.*;
import balok.causality.*;

aspect ParallelFutureAdvice {
    public TaskView ParallelFuture.stamp;
    
    pointcut onPut(HabaneroActivity act, HjFuture promise):
        call (public void notifyPrePut(HjFuture, ..))
        && target(act)
        && args(promise, ..);

    pointcut onGet(HabaneroActivity act, HjFuture promise):
        call (public void notifyPostGet(HjFuture))
        && target(act)
        && args(promise);

    before(HabaneroActivity act, HjFuture promise): onPut(act, promise) {
        act.tracker.produceEvent();
        ((ParallelFuture)promise).stamp = act.tracker.createTimestamp();
    }

    after(HabaneroActivity act, HjFuture promise): onGet(act, promise) {
        act.tracker.join(((ParallelFuture)promise).stamp);
    }

}

