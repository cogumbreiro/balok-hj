package balok.hj;

import edu.rice.hj.api.*;
import edu.rice.hj.runtime.baseruntime.*;

aspect FinishAdvice {

    pointcut onFinishStart(HabaneroActivity act):
        call (public FinishState startFinish())
        && target(act);

    pointcut onFinishEnd(HabaneroActivity act):
        call (public void stopFinish() throws SuspendableException)
        && target(act);

    before(HabaneroActivity act): onFinishStart(act) {
        act.tracker.onStartFinish();
    }

    before(HabaneroActivity act): onFinishEnd(act) {
        act.tracker.onEndFinish();
    }
}