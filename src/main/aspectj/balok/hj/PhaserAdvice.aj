package balok.hj;

import edu.rice.hj.api.*;
import edu.rice.hj.runtime.baseruntime.*;
import java.util.*;

aspect PhaserAdvice {

    pointcut onCreate(HabaneroActivity act, HjPhaser ph):
        call (public void notifyPhaserCreation(HjPhaser, HjPhaserMode))
        && target(act)
        && args(ph, HjPhaserMode);

    pointcut onRegister(HabaneroActivity parent, HabaneroActivity child, List<HjPhaserPair> phs):
        call (private void onPhasersReady(HabaneroActivity, HabaneroActivity, List<HjPhaserPair>))
        && target(BaseRuntime)
        && args(parent, child, phs);

    pointcut onSignal(HabaneroActivity act, HjPhaser ph, boolean isDrop):
        call(public void notifySignal(HjPhaser, boolean))
        && target(act)
        && args(ph, isDrop);

    pointcut onWait(HabaneroActivity act, HjPhaser ph):
        call(public void notifyWait(HjPhaser))
        && target(act)
        && args(ph);

    before(HabaneroActivity act, HjPhaser ph): onCreate(act, ph) {
        act.tracker.createPhaser(ph);
    }

    before(HabaneroActivity parent, HabaneroActivity child, List<HjPhaserPair> phs) : onRegister(parent, child, phs) {
        child.tracker.register(parent.tracker, phs);
    }

    before(HabaneroActivity act, HjPhaser ph, boolean isDrop) : onSignal(act, ph, isDrop) {
        if (!isDrop) {
            act.tracker.signal(ph);
        }
    }

    before(HabaneroActivity act, HjPhaser ph) : onWait(act, ph) {
        act.tracker.await(ph);
    }
}