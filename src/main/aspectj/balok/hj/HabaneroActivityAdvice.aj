package balok.hj;

import edu.rice.hj.api.*;
import edu.rice.hj.runtime.baseruntime.*;

aspect HabaneroActivityAdvice {
    // The tracker of each activity
    public ActivityTracker HabaneroActivity.tracker;

    pointcut onSpawn(HabaneroActivity parent, HabaneroActivity child):
        call (public void notifyActivitySpawn(HabaneroActivity, ..))
        && target(parent)
        && args(child, ..);

    pointcut onRunKernel():
        call (public void runKernel(HjSuspendable))
        && target(edu.rice.hj.runtime.baseruntime.BaseRuntime);

    pointcut onActivityEnd(HabaneroActivity act):
        call (private void setFinished())
        && target(act);

    // Setting up the root task
    before(SuspendableActivity act): set(private SuspendableActivity initialActivity) && args(act) {
        if (act != null) {
            act.tracker = new ActivityTracker();
        }
    }

    before(): onRunKernel() {
        AppProperties.getDetectionStrategy().start();
    }
    after(): onRunKernel() {
        try {
            AppProperties.getDetectionStrategy().stop().get();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    after(HabaneroActivity act): onActivityEnd(act) {
        act.tracker.onEndActivity();
    }

    // When the base-runtime spawns a task
    before(HabaneroActivity parent, HabaneroActivity child): onSpawn(parent, child) {
        child.tracker = parent.tracker.createChild();
        parent.tracker.afterSpawn();
    }
}

