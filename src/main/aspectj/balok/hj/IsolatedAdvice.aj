package balok.hj;

import balok.causality.*;
import edu.rice.hj.api.*;
import edu.rice.hj.runtime.baseruntime.*;
import java.util.*;
import java.util.concurrent.*;

aspect IsolatedAdvice {
    private TaskView isolatedTimestamp;
    private ConcurrentReferenceHashMap<Object, TaskView> locks = new ConcurrentReferenceHashMap<>();

    pointcut onIsolatedStart(HabaneroActivity act, Object[] objects):
        call(public void notifyPreIsolated(Object[]))
        && target(act)
        && args(objects);

    pointcut onIsolatedEnd(HabaneroActivity act, Object[] objects):
        call(public void notifyPostIsolated(Object[]))
        && target(act)
        && args(objects);

    before(HabaneroActivity act, Object[] objects): onIsolatedStart(act, objects) {
        if (objects == null) {
            // Global isolated block
            if (isolatedTimestamp != null) {
                act.tracker.join(isolatedTimestamp);
            }
        } else {
            // isolated on specific objects
            for (Object obj : objects) {
                if (obj == null) {
                    continue;
                }
                TaskView ts = locks.get(obj);
                if (ts != null) {
                    act.tracker.join(ts);
                }
            }
        }
    }

    before(HabaneroActivity act, Object[] objects): onIsolatedEnd(act, objects) {
        TaskView ts = act.tracker.createTimestamp();
        if (objects == null) {
            isolatedTimestamp = ts;
        } else {
            // isolated on specific objects
            for (Object obj : objects) {
                if (obj == null) {
                    continue;
                }
                locks.put(obj, ts);
            }
        }
        act.tracker.produceEvent();
    }
}